var i = 0;
function increaseBadge() {
    document.getElementById("shopping-cart-badge").innerHTML = ++i;
    document.getElementById("shopping-cart-badge").style.visibility = "visible";
}

function clickOnImage(element){
    var image=document.getElementById(element.id).src;
    document.getElementById("main-image").src=image;
}


var imageSource = null;
function mouseOverFunction(){
    var image = document.getElementsByClassName("main-image");
    imageSource = image[0].src;
    image[0].src="https://s3.amazonaws.com/busites_www/takamine/content/products/p6jc-bsb_thumbnail.png";
}

function mouseLeftFunction() {
    var x = document.getElementsByClassName("main-image");
    x[0].src = imageSource;
}

function shippingSelectChange() {
    var selectBox = document.getElementById("shipping-country-select").value;
    var items = document.getElementsByClassName("item-name");
    if (selectBox.localeCompare("None") != 0)
    {
        for (var i = 0; i < items.length; ++i)
        {
            if (i % 2 == 0)
            {
                items[i].innerHTML = selectBox;
            }
        }
    }
}